# History (Change Log)

## Table of Contents

1. [GitLab Revisions](#gitlab-revisions)
    * [Monday, September 7<sup>th</sup>, 2020](#monday-september-7supthsup-2020)
    * [Monday, July 20<sup>th</sup>, 2020](#monday-july-20supthsup-2020)
    * [Sunday, July 19<sup>th</sup>, 2020](#sunday-july-19supthsup-2020)
    * [Monday, May 11<sup>th</sup>, 2020](#monday-may-11supthsup-2020)
1. [tonymacx86 Forum Post Revisions](#tonymacx86-forum-post-revisions)
    * [Revision 20 (Wednesday, April 29<sup>th</sup>, 2020)](#revision-20-wednesday-april-29supthsup-2020)
    * [Revision 19 (Date Unknown)](#revision-19-date-unknown)
    * [Revision 18 (Date Unknown)](#revision-18-date-unknown)
    * [Revision 17 (Date Unknown)](#revision-17-date-unknown)
    * [Revision 16 (Date Unknown)](#revision-16-date-unknown)
    * [Revision 15 (Date Unknown)](#revision-15-date-unknown)
    * [Revision 14 (Date Unknown)](#revision-14-date-unknown)
    * [Revision 13 (Date Unknown)](#revision-13-date-unknown)
    * [Revision 12 (Date Unknown)](#revision-12-date-unknown)
    * [Revision 11 (Date Unknown)](#revision-11-date-unknown)
    * [Revision 10 (Date Unknown)](#revision-10-date-unknown)
    * [Revision 9 (Date Unknown)](#revision-9-date-unknown)
    * [Revision 8 (Date Unknown)](#revision-8-date-unknown)
    * [Revision 7 (Date Unknown)](#revision-7-date-unknown)
    * [Revision 6 (Date Unknown)](#revision-6-date-unknown)
    * [Revision 5 (Date Unknown)](#revision-5-date-unknown)
    * [Revision 4 (Date Unknown)](#revision-4-date-unknown)
    * [Revision 3 (Date Unknown)](#revision-3-date-unknown)
    * [Revision 2 (Date Unknown)](#revision-2-date-unknown)
    * [Revision 1 (Friday, December 20<sup>th</sup>, 2019)](#revision-1-friday-december-20supthsup-2019)

## GitLab Revisions

### Monday, September 7<sup>th</sup>, 2020

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Another trivial revision:  

 - HISTORY.md:  
   - Table of Contents:  
     - Section heading:  
       - Corrected it from a top-level heading to a second-level one.  
       - Fixed its capitalization.  

### Monday, July 20<sup>th</sup>, 2020

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_Very_ minor revision:  

 - HISTORY.md:  
   - Fixed all non-list paragraphs' indentation.  

### Sunday, July 19<sup>th</sup>, 2020

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Minor revision:  

 - README.md:  
   - Inserted a prominent note on how the display that came with my machine as ordered requires a discrete GPU under its 'Components' section's 'Base Laptop Configuration' sub-section's list of noted order customizations.  

### Monday, May 11<sup>th</sup>, 2020

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;First GitLab revision.  (🎉🎉!!)  

 - README.md:  
   - Made a few minor wording tweaks/changes/enhancements.  
   - Moved the top-level 'Previous Discussion' and 'Useful Links' sections under a new top-level heading titled 'References.'  
   - Noticed and corrected that, when adding link to Intel's thermal paste application guide to this repository's read-me's section of useful links, I accidentally added it to its section on prior discussion instead.  
   - Split its change log section out into its own, separate file (this one.)  
 - HISTORY.md:  
   - Inverted this change log's entries' sort order from chronological to reverse chronological&thinsp;—&thinsp;to put its newest entries at the top, naturally.  
   - Promoted the one-item bulleted list under this change log's entry for revision 1 out of bulleted-list–hood since it'd read better that way.  
   - Fixed a small erratum under this change log's entry for revision 14 in order to:  
     - Reflect a fix made to what's now in this repository's read-me in revision 15.  
     - Ensure that all mentions of the AMD Radeon Pro WX 4170 mobile attribute themselves to it correctly.  
   - Standardized the capitalization of revision 18's bulleted list's first item's child bullet points towards sentence casing.  
   - Removed the 'TODO' note I'd left at the bottom of this change log to remind myself to add what date-/timestamps I could to its entries when it was still part of this repository's read-me, as I've now done that.  

Known Issue:  

 - README.md:  
   - The first bullet point under the 'Resources' section's 'Useful Links' sub-section is supposed to be a reference-style Markdown link, but those apparently don't show up as items in bulleted lists.  (See issue #1.)  

## tonymacx86 Forum Post Revisions

### Revision 20 (Wednesday, April 29<sup>th</sup>, 2020)

 - Added an update note after the additional note directly following note 1 regarding confirmation of what was discussed in the latter.  
 - Documented that I didn't need the trimmable thermal foam padding for insulating the RAM from heat coming from the system board, but _did_ need it to make up for some differences in thermal padding between the old and new heat sinks.  
 - Noted that the WWAN antennas come installed even on models that don't have the card for them to plug into; they connect to a passthrough circuit on the system board that _then_ connects them to that card.  
 - _Slightly_ modified note 3 to edit in a _small_ clarifying distinction reflecting build status.  
 - Also added a reference to a later status-update post in this thread that notes I need one more part.  
 - Added a link to Intel's thermal paste application guide to this post's section of useful links.  

### Revision 19 (Date Unknown)

 - Updated note 3 to further reflect general machine and part order status; all parts have now arrived.  I just need to put everything together now.  

### Revision 18 (Date Unknown)

 - Added links to the product pages from whence I purchased my:  
   - Solid-state drive.  
   - RAM thermal pad.  
 - Updated note 3 to reflect general machine and part order status.  

### Revision 17 (Date Unknown)

 - Added a link to the Mouser Electronics product page where I purchased my third WiFi/Bluetooth antenna.  

### Revision 16 (Date Unknown)

 - Added a link to the eBay product page where I purchased my '12+6'-pin–to–m.2 2230 Type E adapter.  

### Revision 15 (Date Unknown)

 - Corrected a small graphics card detail I apparently overlooked while composing revision 14.  
 - Corrected the type of antennas the Broadcom BCM94360CSAX uses to [one that _actually exists_](https://www.snbforums.com/threads/intel-7260-m-fl-plug.15161/#post-307752).  Also break this set of parts out from an aggregate listing and into a somewhat itemized breakdown (of what I can reuse from my stock machine and what I have to get separately.)  
 - Added a few more parts to the list of ones I'm using:  
   - The appropriate OEM heat sink for my graphics card.  
   - Some thermal foam required while replacing the ZBook 17 G6's primary RAM modules.  

### Revision 14 (Date Unknown)

 - Updated my graphics card discussion comments/notes again, this time to correct the WX 7100's wattage to what AMD specifies and adjust my comparison of it with the 4170 accordingly.

### Revision 13 (Date Unknown)

 - Reverted the changes introduced by revision 12, changing graphics cards back to the AMD Radeon Pro WX 4170; [it turns out that the ZBook 17 line takes MXM 3.x Type B cards](https://www.tonymacx86.com/threads/my-first-hackintosh-wip-the-build-hp-zbook-17-g6.289048/post-2060278).  
 - Updated my comments discussing graphics cards to correct why I couldn't use the WX _7100_.  

### Revision 12 (Date Unknown)

 - Changed graphics cards (_again_,) this time from the AMD Radeon Pro WX 4170 to the WX 4150.  As also noted in this post's body, some informative discussion with Har0n further along in this thread&thinsp;—&thinsp;the relevant part starts [here](https://www.tonymacx86.com/threads/my-first-hackintosh-wip-the-build-hp-zbook-17-g6.289048/post-2059608)&thinsp;—&thinsp;raised notice of the incompatibility of the former cad with my machine.  
 - Updated the wording of a part-sourcing detail to read more consistently with respect to other such notes here.  

### Revision 11 (Date Unknown)

 - Moved the 'Summary' section after the 'Comments' one.  

### Revision 10 (Date Unknown)

 - _Very_ minor wording update to a footnote to reflect machine and replacement part order status.  

### Revision 9 (Date Unknown)

 - Fixed the listing for the type of adapter I need for my wireless card.  

### Revision 8 (Date Unknown)

 - Added a link to the eBay product page where I purchased my Broadcom BCM94360CSAX WiFi/Bluetooth card.  

### Revision 7 (Date Unknown)

 - Added a link to the eBay product page where I purchased my AMD Radeon Pro WX 4170 mobile.  

### Revision 6 (Date Unknown)

 - Specified which type of WiFi/Bluetooth antennas I need (M.FL, as listed.)  

### Revision 5 (Date Unknown)

 - Added thin spaces around the multiplication sign in this post's listing of my machine's display resolution.  

### Revision 4 (Date Unknown)

 - Changed the second bullet under 'Previous Discussion' to say that additional discussion of the bullet item before it occurred elsewhere but not state exactly where.  

### Revision 3 (Date Unknown)

 - Updated the wording of a note to bring it up to date with respect to purchase timing.  

### Revision 2 (Date Unknown)

 - Attempted to restore a link under 'Previous Discussion' to a thread elsewhere on the Internet that contains additional discussion concerning the first item in that list.  

### Revision 1 (Friday, December 20<sup>th</sup>, 2019)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Initial tonymacx86 forum post.  (🎉!)  