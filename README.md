<aside><i>Author's Note:</i>  Document structure and formatting inspired by <a href="https://www.tonymacx86.com/threads/read-me-first-required-tonymacx86-user-builds-template.25660/">the standard tonymacx86 User Builds Template</a> even though this is a laptop (well, 'mobile workstation,') build.  Regardless, said template was useful enough to riff off of here.  </aside>

---

<div align="center">
  <h1>RandomDSdevel's HP ZBook 17 G6 Hackintosh Build</h1>
  6CK22AV_MB&thinsp;—&thinsp;Intel Core i9-9880H (9th-Generation)<br />
  <s>8 GiB</s>32 GiB 2,666-MHz DDR4 SDRAM<sup>1</sup><br />
  <s>NVIDIA Quadro T1000 (4 GiB dedicated GDDR5 VRAM)</s>AMD Radeon Pro WX 4170 (4 GiB dedicated GDDR5 VRAM)<sup>1</sup>
  <br /><br />
  <figure>
    <img src="/Assets/Images/HP ZBook 17 G6.png" alt="HP ZBook 17 G6" title="HP ZBook 17 G6" />
    <figcaption>A picture of the machine from HP's web site.  </figcaption>
  </figure>
</div>

(<sup>1</sup>&thinsp;As one might suspect, the components I've struck through are what the laptop came with, whereas those after them are the parts I'm replacing them with. Also listed below in the next section.)  

## Table of Contents
<!--Generated manually, but based on initially running GitHub user alexharv074's 'markdown_toc' project's Ruby '`mdtoc`' script (hosted at the project's GitHub page at <https://github.com/alexharv074/markdown_toc>) on this document.  Doing that as opposed to using GitLab's built-in Markdown '`[[_TOC_]]`' macro provided me a good trade-off between automatic ToC generation and greater manual control over layout and which headings got included.  -->

1. [Components](#components):  
    * [Base Laptop Configuration](#base-laptop-configuration)
    * [Replacement Parts](#replacement-parts)
1. [Comments](#comments)
1. [Summary](#summary)
1. [Hardware Reconfiguration](#hardware-reconfiguration)
1. [System OS and Software Setup and macOS Installation](#system-os-and-software-setup-and-macos-installation)
1. [What Works](#what-works)
1. [What Doesn't Work](#what-doesnt-work)
1. [Benchmarks](#benchmarks)
1. [Final Thoughts and Considerations](#final-thoughts-and-considerations)
1. [Resources](#resources):  
    * [Previous Discussion](#previous-discussion)
    * [Useful Links](#useful-links)

## Components

### Base Laptop Configuration

#### HP ZBook 17 G6
HP, Model '6CK22AV_MB'

Equipped with:  

 - An 8-core, 2.3-GHz Intel Core i9-9880H processor<sup>2</sup> (Up to 4.8 GHz with Turbo Boost, 16 MB cache)
 - Intel UHD Graphics 630

(<sup>2</sup>&thinsp;The i9-9980HK wasn't a build-to-order option, unfortunately.)  
(Additional note:  [The machine's maintenance and service guide][Maintenance and Service Guide] doesn't have instructions on how to replace the CPU, so I assume that, like most laptops' CPUs, this chip is soldered to the motherboard instead of being a removable part placed in a socket there.)  (Update:  This does appear to be the case after getting that far/deep into the build.)  

Customized to order to also come with:  
 - Intel vPro/AMT (Active Management Technology) (Comes with the i9-9880H processor; left enabled.)  
 - 8 GiB 2,666-MHz DDR4 SDRAM (Note:  To be replaced.)  
 - A 17.3"-diagonal UHD B-LED UWVA anti-glare 'DreamColor' display (Self-calibrating) (3840&thinsp;✕&thinsp;2160 pixel resolution, 400 nits of brightness)  (N. b.:  This particular display _**requires the presence of an installed discrete GPU.**_)  
 - An Nvidia Quadro T1000 graphics card (4 GiB of dedicated GDDR5 VRAM) (Note:  To be replaced.)  
 - A 256-GB TLC (triple-level cache) SATA Ⅲ SSD (Note:  To be replaced.)  
 - A dual-layer Blu-ray R/RE and DVD±RW 'SuperMulti' drive
 - An 'Intel Dual Band Wi-Fi 6 AX200 (2x2) and Bluetooth 5 combo, vPro' (Note:  To be replaced.)  
 - No fingerprint reader (I'm not waiting around for the community to create a '`.kext`' or '`.dext`' that emulates Apple's T-series coprocessors.)  
 - The following hard recovery media resources:  
   - A 'Drivers for Windows 10' disc.  
   - A Windows 10 Pro 'Operating System Recovery DVD.'  

### Replacement Parts

#### AMD Radeon Pro WX 4170 (Mobile)
4 GiB dedicated GDDR5 VRAM
MXM 3.0 Type B PCIe 3.0 x16 card

https://www.ebay.com/itm/HP-ZBOOK-17-G5-AMD-RADEONPRO-WX-4170-4GB-LAPTOP-VIDEO-CARD-916613-002-L30654-001/333275848289?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649

#### Heat Sink for Use in Models With Discrete Graphics F16M Memory
HP part number 'L30393-001'

Sourced directly from [the HP Parts Store](https://parts.hp.com/Hpparts/Default.aspx?mscssid=9BEAF7C665FF482391D863A86B82D48A&cc=US&lang=EN&jumpid=); availability scarce&thinsp;—&thinsp;on back-order, I should note&thinsp;—&thinsp;when I bought it and subject to change.  

(Addendum:  See also my comment regarding the thermal padding included later in this list.)  

#### [Crucial 32GB Kit of 2 16-GiB 2,666-MHz DDR4 SDRAM SODIMMs](https://www.crucial.com/usa/en/hp-zbook-17-g6-mobile-workstation/CT16162181)

#### Arctic Thermal Pad
50 mm ✕ 50 mm ✕ 1.0 mm

~~Insulates the RAM, presumably from other heat sources like the system board and graphics card.~~  

~~(Specified as required while replacing the primary RAM module by [the HP ZBook 17 G6's maintenance and service guide][Maintenance and Service Guide].)~~  

It turns out that the existing primary RAM actually _wasn't_ insulated with any thermal padding I'd need to replace, so I didn't need to do that.  I _did_ use this later for something else, though, namely being _extra_ careful and making up for some differences in thermal padding between the old and new heat sinks where the former had some where the pre-installed graphics card has a few components but the latter did not where my new card had similarly placed components.  (I'm not sure what those _are_, exactly, except that they aren't anything major, but I thought it would be better to be safe than sorry.)  

Source:  [Amazon](https://www.amazon.com/gp/product/B00UYTTDO6/ref=ppx_od_dt_b_asin_title_s00?creativeASIN=B00UYTTDO6&linkCode=w61&imprToken=295s0TwaTageQ2NloN8T2Q&slotNum=0&ie=UTF8&psc=1?ie=UTF8&tag=tonymacx86com-20)

#### [4-TB 2.5-inch SATA-Ⅲ Samsung 860 Pro SSD](https://www.samsung.com/us/computing/memory-storage/solid-state-drives/ssd-860-pro-2-5--sata-iii-4tb-mz-76p4t0bw/)
Source:  [NewEgg](https://www.newegg.com/samsung-860-pro-series-4tb/p/N82E16820147706?Description=SAMSUNG%20860%20Pro%20Series%202.5%22%204TB%20SATA%20III%20V-NAND%202-bit%20MLC%20Internal%20Solid%20State%20Drive%20%28SSD%29%20MZ-76P4T0E)

#### Broadcom BCM94360CSAX
802.11ac/Bluetooth 4.2 '12+6'-pin WLAN card

https://www.ebay.com/itm/MacBook-Pro-13-Retina-Airport-and-Bluetooth-Wireless-Card-for-A1502-2013-2014/163190086820?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649

#### '12+6'-pin–to–m.2 2230 Type E Adapter
https://www.ebay.com/itm/12-6-Pin-to-M-2-Key-A-E-Adapter-Cable-for-Wireless-WIFI-Card-for-Mac-OS-andU4V6/174095941265

#### Antennas
<s>M.FL</s>MHF4, ✕&thinsp;3:  
 - 2 for WiFi:  
   - 1 for 2.4-GHz transmissions
   - 1 for 5-GHz transmissions
 - 1 for Bluetooth

Sources:  
 - 2 come with the ZBook 17 G5's included WLAN card.  
 - Mouser Electronics (1 Molex edge-fedd 300-mm 2.4- to 5-GHz MHF4 antenna; product page [here](https://www.mouser.com/ProductDetail/538-204281-1300).)  

(Notes:  
 - The WiFi/Bluetooth combo card that comes with the ZBook G6 only has the two antennas it comes with, but that this should work fine:  I can just take one of the slots meant for the optional wireless broadband WWAN card's antennas, too, or something.  I'm not getting anything like it now and probably never will, anyway.  
 - I'll actually have to take those WWAN antennas out; they still come with models without a WWAN card installed, as they plug into a passthrough circuit on the system board that itself connects to that card. So, I actually have four antennas inside out of the box, but only two use the WLAN-compatible frequencies, as far as I can figure, so those are all I can borrow.  
 - [Apparently, M.FL is what one company would call their line of MHF4-compatible products…if they even _carried_ any](https://www.snbforums.com/threads/intel-7260-m-fl-plug.15161/#post-307752).)  

## Comments

 - I _was_ going to get an HP ZBook 17 G**_<u>5</u>_**, but I waited too long to buy it, and now it's been discontinued.  This actually had me wringing my hands for a bit, as I mistakenly had the impression that the G5 was the last ZBook 17 to have an optical drive, but I know now that one's still available on the G6, so no harm done.  
 - I originally wanted to select the AMD Radeon Pro WX 7100 mobile (instead of the 4170 mobile) as my replacement graphics card, but this higher-end model is a 100-watt MXM 3 Type B card that I don't even know would be compatible with my machine; I've certainly never seen a compatible heat sink for such a configuration.  The AMD Radeon Pro WX 4170 mobile is an MXM 3 Type B card that only draws 50 or 60 watts and which I know should work&thinsp;—&thinsp;it was a customize-to-order part available with the ZBook G5&thinsp;—, so that's what I'm going with.  

## Summary

TODO/TBD<sup>3</sup>

## Hardware Reconfiguration

TODO

## System OS and Software Setup and macOS Installation

TODO/TBD<sup>3</sup>

## What Works

TBD<sup>3</sup>

## What Doesn't Work

TBD<sup>3</sup>

## Benchmarks

TODO/TBD<sup>3</sup> (These are optional, however, though I bet folks would have interest in seeing them.)  

(<sup>3</sup>&nbsp;I have all of the parts I need now&nbsp;—&nbsp;edit: except as per [post #30](https://www.tonymacx86.com/threads/my-first-hackintosh-wip-the-build-hp-zbook-17-g6.289048/post-2113143)&thinsp;—, but I haven't assembled/replaced them all yet, so I can't test to see what works and what doesn't until I do.  I'll update these sections after I've finished setup.)  

## Final Thoughts and Considerations

TODO

## Resources

### Previous Discussion

 - ['Best Broadcom m.2 WiFi/Bluetooth Card for an HP ZBook 17 G5? — tonymacx86 Forums'](https://www.tonymacx86.com/threads/best-broadcom-m-2-wifi-bluetooth-card-for-an-hp-zbook-17-g5.286125/post-2043986) (From when I still planned on buying the G5.)  
 - Other discussion of that same thread elsewhere on the Web.  <!--Add a Git branch where this bullet is replaced by actual references to the relevant Reddit posts/threads to my repository.  -->
 - The following related posts in [the '[Guide] HP ProBook/EliteBook/Zbook using Clover UEFI hotpatch' thread](https://www.tonymacx86.com/threads/guide-hp-probook-elitebook-zbook-using-clover-uefi-hotpatch.261719/):  
   - By me, RandomDSdevel:  
     - [Post #1,629](https://www.tonymacx86.com/threads/guide-hp-probook-elitebook-zbook-using-clover-uefi-hotpatch.261719/post-2032622)
     - [Post #1,699](https://www.tonymacx86.com/threads/guide-hp-probook-elitebook-zbook-using-clover-uefi-hotpatch.261719/post-2044451)

### Useful Links

 - [The HP ZBook 17 G6 Maintenance and Service Guide][Maintenance and Service Guide]
 - [Intel's thermal paste application guide](https://www.intel.com/content/www/us/en/gaming/resources/how-to-apply-thermal-paste.html)

<!--Per Michael 'dnsmichi' Friedrich, reference-style Markdown links' targets:  

     - _Must_ reside at the _end_ of a Markdown file.  
     - Are _not_ rendered there.  

Confer:  <https://forum.gitlab.com/t/cant-make-a-reference-style-link-thats-part-of-a-bulleted-list-render-in-a-markdown-file-on-gitlab-com>, specifically [this post](https://forum.gitlab.com/t/cant-make-a-reference-style-link-thats-part-of-a-bulleted-list-render-in-a-markdown-file-on-gitlab-com/37484).  -->

[Maintenance and Service Guide]:  <http://h10032.www1.hp.com/ctg/Manual/c06422401> "The HP ZBook 17 G6 Maintenance and Service Guide"